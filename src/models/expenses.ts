export default class Expenses {
    category: string;
    description: string;
    amount: number;
}