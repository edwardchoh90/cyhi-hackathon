export default class Income {
    category: string;
    description: string;
    amount: number;
}