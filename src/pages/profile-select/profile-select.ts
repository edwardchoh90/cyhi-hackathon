import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {ListPage} from "../list/list";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the ProfileSelectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-profile-select',
  templateUrl: 'profile-select.html',
})
export class ProfileSelectPage {

  profiles=[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillLoad() {


    this.profiles = [
        {
            image:"assets/imgs/kid2.jpg",
            name:"Sana"
        },
        {
            image:"assets/imgs/Alicia.jpg",
            name:"Alicia"
        },
    ]
  }

    goToHomeScreen(name){
      this.navCtrl.setRoot(TabsPage,{name:name})
    }

}
