import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { AppServicesProvider } from "../../app-services/app-services";
import Income from '../../models/income';

/**
 * Generated class for the StoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-story',
  templateUrl: 'story.html',
})
export class StoryPage {
  @ViewChild('lineCanvas') lineCanvas;
  @ViewChild('lineCanvas2') lineCanvas2;
  @ViewChild('lineCanvas3') lineCanvas3;
  lineChart: any;
  lineChart2: any;
  lineChart3: any;
  goal = 0;
  currentBalance = 0;
  expenses = [];
  chore = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public appService:AppServicesProvider) {
  }

  ionViewDidLoad() {
    this.currentBalance = 20;
    var weekLabels = ["Week 1", "Week 2", "Week 3", "Week 4", "Week 5", "Week 6", "Week 7", "Week 8", "Week 9"];
    var baseSet = {
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(75,192,192,0.4)",
      borderCapStyle: 'butt',
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "rgba(75,192,192,1)",
      pointBackgroundColor: "#fff",
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      spanGaps: false
    }
    
    var pocketMoney = this.appService.pocketMoney;
    this.expenses = this.appService.getExpenses()
    this.chore = this.appService.getChore();
    this.goal = this.appService.getGoal() || 100;
    
    var totalExpense = this.expenses.reduce((acc, curr) => curr.checked ? acc + (+curr.amount) : acc + 0, 0);
    console.warn('DEBUG chore:', this.chore);
    var totalChore = this.chore.reduce((acc, curr) => curr.checked ? acc + (+curr.amount) : acc + 0, 0);
    console.warn('DEBUG pocketMoney:', pocketMoney);
    console.warn('DEBUG totalChore:', totalChore);
    var netIncome = pocketMoney - totalExpense;
    console.warn('DEBUG netIncome:', netIncome);
    console.warn('DEBUG totalExpense:', totalExpense);

    var goalDataSet = Object.assign({
      label: "Goal",
      borderColor: "rgba(255,0,255,0.3)",
      data: weekLabels.map((it) => this.goal),
      borderDash: [15, 5],
    }, baseSet);
    var normalDataSet = Object.assign({
      label: "Normal",
      borderColor: "rgba(75,192,192,1)",
      data: weekLabels.map((it, ind) => { return this.currentBalance + ind*netIncome }) // [20, 30, 40, 50, 60, 70, 80, 90, 100]
    }, baseSet);
    console.warn('DEBUG less expense:', normalDataSet.data);
    var lessExpenseDataSet = Object.assign({
      label: "Less Expense",
      borderColor: "rgba(255,255,0,0.3)",
      data: weekLabels.map((it, ind) => { return this.currentBalance + ind*(netIncome+totalExpense) }) // [20, 40, 60, 80, 100, 120, 140, 160, 180]
    }, baseSet);
    console.warn('DEBUG less expense:', lessExpenseDataSet.data);
    var bestDataSet = Object.assign({
      label: "Complete Chores + Less Expense",
      borderColor: "rgba(192,192,192,0.3)",
      data: weekLabels.map((it, ind) => { 
        if (ind >= 1) return this.currentBalance + ind*(netIncome+totalExpense) + totalChore;
        return this.currentBalance + ind*(netIncome+totalExpense) 
      })
    }, baseSet)
    console.warn('DEBUG best:', bestDataSet.data);
    console.log('ionViewDidLoad StoryPage');

    var options = {
      scales: {
        yAxes: [{
          ticks: {
            max: bestDataSet.data[bestDataSet.data.length-1]
          }
        }]
      },
      layout: {
        padding: {
          left: 50,
          right: 50,
          top: 50,
          bottom: 50
        }
      }
    }

    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: weekLabels,
        datasets: [
          goalDataSet,
          normalDataSet
        ]
      },
      options: options
    });

    this.lineChart2 = new Chart(this.lineCanvas2.nativeElement, {
      type: 'line',
      data: {
        labels: weekLabels,
        datasets: [
          goalDataSet,
          normalDataSet,
          lessExpenseDataSet
        ]
      },
      options: options
    });

    this.lineChart3 = new Chart(this.lineCanvas3.nativeElement, {
      type: 'line',
      data: {
        labels: weekLabels,
        datasets: [
          goalDataSet,
          normalDataSet,
          lessExpenseDataSet,
          bestDataSet
        ]
      },
      options: options
    });
  }

}
