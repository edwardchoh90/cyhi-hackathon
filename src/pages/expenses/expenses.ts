import { Component } from '@angular/core';
import {  NavController, NavParams, ModalController } from 'ionic-angular';
import { AddIncomePage} from "../add-income/add-income";
import { AppServicesProvider } from '../../app-services/app-services'
/**
 * Generated class for the ExpensesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-expenses',
  templateUrl: 'expenses.html',
})
export class ExpensesPage {

    expenses = [];
    constructor(public navCtrl: NavController, public navParams: NavParams,
                public modal:ModalController, public appService:AppServicesProvider) {
    }
    ionViewWillEnter() {
        this.expenses = this.appService.getExpenses()
    }

    addExpense(){
        let expenseModal = this.modal.create(AddIncomePage,{title:'Add Expense'})

        expenseModal.present()

        expenseModal.onDidDismiss((data) => {

            if (data){
                data.checked = true;
                this.expenses.push({amount:data.amount,description:data.description});
                console.log("THIS CHORES",this.expenses)
            }
        })

    }

    ionViewWillLeave(){
        this.appService.setExpenses(this.expenses);
    }

    updateCheckbox(index,checkedStatus){
        this.expenses[index].checked = !checkedStatus
    }


}
