import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import {AddIncomePage} from "../add-income/add-income";
import { AppServicesProvider  } from '../../app-services/app-services'

/**
 * Generated class for the IncomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-income',
  templateUrl: 'income.html',
})
export class IncomePage {


  pocketMoney = 0;
  chores = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public modal:ModalController,
              public appService:AppServicesProvider) {
  }
  ionViewWillEnter() {
   this.chores  = this.appService.getChore()
   this.pocketMoney = this.appService.pocketMoney;
  }

  addIncome(){
      let incomeModal = this.modal.create(AddIncomePage,{title:'Add Income'})

      incomeModal.present()

      incomeModal.onDidDismiss((data) => {

          if(data){
              data.checked = true;
              this.chores.push({amount:data.amount,description:data.description});
              console.log("THIS CHORES",this.chores)
          }
      })

  }

  ionViewWillLeave(){
      this.appService.setChore(this.chores);
  }

    updateCheckbox(index,checkedStatus){


        this.chores[index].checked = !checkedStatus
    }

}
