import { Component } from '@angular/core';


import { ListPage } from '../list/list';
import {IncomePage} from "../income/income";
import {ExpensesPage} from "../expenses/expenses";
import {GoalPage} from "../goal/goal";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ListPage;
  tab2Root = IncomePage;
  tab3Root = ExpensesPage;
  tab4Root = GoalPage


  constructor() {

  }
}
