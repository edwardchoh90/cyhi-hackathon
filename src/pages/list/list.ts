import { Component, OnInit,NgZone } from '@angular/core';
import Income from '../../models/income';
import Expenses from '../../models/expenses';
import { NavController, ModalController } from 'ionic-angular';
import { StoryPage } from "../story/story";
import { AppServicesProvider } from "../../app-services/app-services";

@Component({
    selector: 'page-home',
    templateUrl: 'list.html'
})
export class ListPage  {


    income;
    expenses;
    balance: number;
    pocketMoney:number;
    goalProgress = 30;
    goal;
    goalImg;
    goalName;

    constructor(public navCtrl: NavController, public modal:ModalController,
                public appService:AppServicesProvider,public zone:NgZone) {

    }

    navToStory(){
        this.navCtrl.push(StoryPage,{story:'this is a soty'})

    }

    ionViewWillEnter() {
        this.goal = this.appService.getGoal();

        this.expenses = this.appService.getExpenses().filter(res=>res.checked);
        this.income = this.appService.getChore().filter(res=>res.checked);

        this.pocketMoney = this.appService.pocketMoney;
        this.balance = this.pocketMoney + this.getBalance();



        if (this.goal){
            this.goal = this.goal[0]
            this.goalImg = "assets/imgs/lagoon2.jpg"
            this.goalProgress = (this.balance / this.goal.goalPrice)*100;
            this.goalName = this.goal.goalName

        }

        this.expenses = this.appService.getExpenses().filter(res=>res.checked);
        this.income = this.appService.getChore().filter(res=>res.checked);

        this.pocketMoney = this.appService.pocketMoney;
        this.balance = this.pocketMoney + this.getBalance();
    }

    getTotal(items) {
        let total = 0;
        for (let i in items){
            total = total + items[i].amount;
            console.log("total",total)
        };

        return total

    }


    getBalance() {
        return   this.getTotal(this.income) - this.getTotal(this.expenses);
    }
}
