import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the AddGoalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-add-goal',
  templateUrl: 'add-goal.html',
})
export class AddGoalPage {

  img="assets/imgs/lagoon2.jpg";
  amount;
  description;
  name;

  constructor(public navCtrl: NavController, public navParams: NavParams, private view:ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddGoalPage');
  }

  dismiss(){
   this.view.dismiss(
       {goalImg:"assets/imgs/lagoon1.jpg",
              goalDescription:this.description,
              goalName:this.name,
              goalPrice:+this.amount})
  }

}
