import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { AddGoalPage } from "./add-goal/add-goal";
import { AppServicesProvider } from "../../app-services/app-services";

/**
 * Generated class for the GoalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-goal',
  templateUrl: 'goal.html',
})
export class GoalPage {


    goals = [];
    goalPrice = "130"
    goalName ="Sunway Lagoon";
    goalDescription = "Have Fun in the theme park with family joy";
    goalImage = "assets/imgs/kid2.jpg"


  constructor(public appService:AppServicesProvider, public navCtrl: NavController, public navParams: NavParams, public modal:ModalController ) {
  }

  ionViewWillLoad() {

      //DUMMY DATA;
      this.goals.push({
          goalName:"Sunway Lagoon",
          goalDescription:'Have Fun in the theme park with family joy',
          goalImg :"assets/imgs/lagoon1.jpg",
          goalPrice:'100'
      })

  }

  i

  createNewGoal(){

      let goalModal = this.modal.create(AddGoalPage);

      goalModal.present();

      goalModal.onDidDismiss((data) => {

          console.log(data)
          if(data){
              this.goals.push(data);
          }


      })

  }

    ionViewWillLeave(){
        this.appService.setGoal(this.goals);
    }

}
