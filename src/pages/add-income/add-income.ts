import { Component } from '@angular/core';
import { NavController, NavParams,ViewController } from 'ionic-angular';

/**
 * Generated class for the AddIncomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-add-income',
  templateUrl: 'add-income.html',
})
export class AddIncomePage {

    amount:number;
    description;
    title;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public view:ViewController) {

    this.title = this.navParams.get('title')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddIncomePage');
  }

  dismiss(){

      if (this.amount==null || this.description == null){
          this.view.dismiss()
      } else {
          this.view.dismiss({amount:+this.amount,description:this.description,checked:true});
      }

  }

}
