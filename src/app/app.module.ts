import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { ListPage } from '../pages/list/list';
import { TabsPage } from '../pages/tabs/tabs';
import { ChorePage } from '../pages/chore/chore'
import { GoalPage } from '../pages/goal/goal'
import { IncomePage } from '../pages/income/income'
import { ExpensesPage } from "../pages/expenses/expenses";
import { StoryPage } from "../pages/story/story";
import { AddIncomePage } from "../pages/add-income/add-income";
import { ProfileSelectPage } from "../pages/profile-select/profile-select";
import { AddGoalPage } from "../pages/goal/add-goal/add-goal";
import { ProgressBarComponent } from "../components/progress-bar/progress-bar";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppServicesProvider } from '../app-services/app-services';


@NgModule({
  declarations: [
    MyApp,
    ListPage,
    TabsPage,
      ChorePage,
      GoalPage,
      IncomePage,
      ExpensesPage,
      StoryPage,
      AddIncomePage,
      ProfileSelectPage,
      AddGoalPage,
      ProgressBarComponent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ListPage,
    TabsPage,
      ChorePage,
      IncomePage,
      GoalPage,
      ExpensesPage,
      StoryPage,
      AddIncomePage,
      ProfileSelectPage,
      AddGoalPage,
      ProgressBarComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppServicesProvider
  ]
})
export class AppModule {}
