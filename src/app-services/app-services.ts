
/*
  Generated class for the AppServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export class AppServicesProvider {

  expensesArray;
  pocketMoney
  choreArray;
  goalArray;
  userName;



  constructor() {
    console.log('Hello AppServicesProvider Provider');
    this.choreArray  = [{
        description: 'Clean up room',
        amount: 20,
        checked: true,
    }, {
        description: 'Help mum to shop',
        amount: 20,
        checked: false,
    },
        {
            description: 'Sweep the Porch',
            amount: 2,
            checked: false,
        }]


    this.expensesArray =  [{
        description: 'Lunch',
        amount: 5,
        checked: true,
    }, {
        description: 'Candy',
        amount: 5,
        checked: false,
    }];

    this.goalArray =       [{
        goalName:"Sunway Lagoon",
        goalDescription:'Have Fun in the theme park with family joy',
        goalImg :"assets/imgs/lagoon1.jpg",
        goalPrice:'100'
    }];

      this.pocketMoney = 15;

  }

  getExpenses(){
    return this.expensesArray;
  }


  setExpenses(expenses){
    this.expensesArray = expenses;
  }


  getChore(){
    return this.choreArray
  }


  setChore(chore){
        this.choreArray = chore;
  }

  getGoal(){
    return this.goalArray;
  }


  setGoal(goal){
    this.goalArray = goal;
  }


}
